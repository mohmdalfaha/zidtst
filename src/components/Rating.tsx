import * as React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import fonts from '../assets/fonts';

export interface RatingProps {
  rate: number;
}

const Rating: React.FunctionComponent<RatingProps> = props => {
  const roundedRate = Math.round(props.rate);
  return (
    <View style={styles.container}>
      <Ionicons
        name="star"
        color={roundedRate === 5 ? '#FFC700' : '#D1D1D1'}
        size={17}
      />
      <Ionicons
        name="star"
        color={roundedRate >= 4 ? '#FFC700' : '#D1D1D1'}
        size={17}
      />
      <Ionicons
        name="star"
        color={roundedRate >= 3 ? '#FFC700' : '#D1D1D1'}
        size={17}
      />
      <Ionicons
        name="star"
        color={roundedRate >= 2 ? '#FFC700' : '#D1D1D1'}
        size={17}
      />
      <Ionicons
        name="star"
        color={roundedRate >= 1 ? '#FFC700' : '#D1D1D1'}
        size={17}
      />
      <Text style={styles.rateText}>{props.rate} out of 5</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  rateText: {
    fontFamily: fonts.regular,
    fontSize: 14,
    lineHeight: 21,
    marginLeft: 9,
  },
});
export default Rating;
