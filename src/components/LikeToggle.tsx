import * as React from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import fonts from '../assets/fonts';

export interface LikeToggleProps {
  onToggle: ((event: GestureResponderEvent) => void) | undefined;
  isLiked: boolean;
  likesNumber: number;
}

const LikeToggle: React.FunctionComponent<LikeToggleProps> = props => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onToggle}>
        {props.isLiked ? (
          <Ionicons name="ios-heart" size={19} color="#EB1747" />
        ) : (
          <Ionicons name="ios-heart-outline" size={19} color="#D1D1D1" />
        )}
      </TouchableOpacity>

      <Text style={styles.numberOfLikes}>
        {props.isLiked ? props.likesNumber + 1 : props.likesNumber}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  numberOfLikes: {
    fontFamily: fonts.medium,
    fontSize: 14,
    marginLeft: 6,
  },
});
export default LikeToggle;
