import {Text, StyleSheet, StyleProp, TextStyle} from 'react-native';
import fonts from '../assets/fonts';

export interface TextLabelProps {
  text: string;
  textStyle?: StyleProp<TextStyle>;
}

const TextLabel: React.FunctionComponent<TextLabelProps> = props => {
  return <Text style={[styles.textLabel, props.textStyle]}>{props.text}</Text>;
};

const styles = StyleSheet.create({
  textLabel: {
    fontFamily: fonts.medium,
    fontSize: 16,
    lineHeight: 24,
  },
});
export default TextLabel;
