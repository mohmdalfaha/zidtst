import * as React from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  StyleProp,
  ViewStyle,
  Dimensions,
  Animated,
  Modal,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Picker} from '@react-native-picker/picker';

import fonts from '../assets/fonts';
import ActionButton from './ActionButton';

export interface ListPickerProps {
  onChange: (item: string) => void;
  selectedItem: string;
  list: Array<string>;
  label: string;
  containerStyle?: StyleProp<ViewStyle>;
}
const ListPicker: React.FunctionComponent<ListPickerProps> = props => {
  const [showPicker, setShowPicker] = React.useState(false);

  return (
    <>
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => setShowPicker(true)}
        style={[styles.container, props.containerStyle]}>
        <Text style={styles.label}>{props.label}</Text>
        <Text style={styles.textInputStyle}>{props.selectedItem}</Text>
        <MaterialIcons
          style={styles.arrowDownIcon}
          name="keyboard-arrow-down"
          size={14}
          color="#000"
        />
      </TouchableOpacity>
      <Modal visible={showPicker}>
        <Picker
          style={{marginTop: 400}}
          selectedValue={props.selectedItem}
          onValueChange={(itemValue, itemIndex) => {
            props.onChange(itemValue);

            setShowPicker(false);
          }}>
          {props.list.map(item => (
            <Picker.Item key={item} label={item} value={item} />
          ))}
        </Picker>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: '#FCFCFC',
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderColor: 'rgba(0, 0, 0, 0.04)',
    borderWidth: 1,
    width: '100%',
  },
  label: {
    fontFamily: fonts.regular,
    fontSize: 16,
    lineHeight: 24,
    color: '#787878',
  },
  textInputStyle: {
    fontFamily: fonts.regular,
    fontSize: 16,
    lineHeight: 24,
    color: '#000',
  },
  pickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  arrowDownIcon: {position: 'absolute', right: 28, top: 33},
});
export default ListPicker;
