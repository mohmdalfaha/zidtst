import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  StyleProp,
  ViewStyle,
} from 'react-native';
import fonts from '../assets/fonts';

export interface LabeledTextInputProps {
  onChangeText: (text: string) => void;
  value: string;
  label: string;
  placeholder?: string,
  containerStyle?: StyleProp<ViewStyle>;
}

const LabeledTextInput: React.FunctionComponent<
  LabeledTextInputProps
> = props => {
  return (
    <View style={[styles.container, props.containerStyle]}>
      <Text style={styles.label}>{props.label}</Text>
      <TextInput
        placeholder={props.placeholder}
        onChangeText={text => props.onChangeText(text)}
        defaultValue={props.value}
        style={styles.textInputStyle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: '#FCFCFC',
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderColor: 'rgba(0, 0, 0, 0.04)',
    borderWidth: 1,
  },
  label: {
    fontFamily: fonts.regular,
    fontSize: 16,
    lineHeight: 24,
    color: '#787878',
  },
  textInputStyle: {
    fontFamily: fonts.regular,
    fontSize: 16,
    lineHeight: 24,
    color: '#000',
  },
});
export default LabeledTextInput;
