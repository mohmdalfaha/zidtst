import * as React from 'react';
import {
  View,
  Text,
  GestureResponderEvent,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import fonts from '../assets/fonts';
import RNShare from '../utilties/ReactNativeShare';

export interface ShareButtonProps {
  message: string;
}

const ShareButton: React.FunctionComponent<ShareButtonProps> = props => {
  const onShare = () => {
    // TODO: add native share sheet functionality here

    RNShare.open({message: props.message});
  };

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onShare}
      style={styles.container}>
      <Text style={styles.text}>Share</Text>
      <Ionicons name="share-social-outline" size={20} color="#512872" />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    width: 114,
    height: 37,
    backgroundColor: '#EDEBF2',
    borderRadius: 48,
  },
  text: {
    fontFamily: fonts.medium,
    fontSize: 14,
  },
});
export default ShareButton;
