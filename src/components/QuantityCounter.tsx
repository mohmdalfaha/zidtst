import * as React from 'react';
import {
  View,
  Text,
  StyleProp,
  TextStyle,
  ViewStyle,
  GestureResponderEvent,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import fonts from '../assets/fonts';

export interface QuantityCounterProps {
  containerStyle?: StyleProp<ViewStyle>;
  value: number;
  onDecrement: ((event: GestureResponderEvent) => void) | undefined;
  onIncreament: ((event: GestureResponderEvent) => void) | undefined;
}

const QuantityCounter: React.FunctionComponent<
  QuantityCounterProps
> = props => {
  return (
    <View style={[styles.container, props.containerStyle]}>
      <TouchableOpacity hitSlop={{right: 30, left: 10, top: 20, bottom: 20}} onPress={props.onIncreament}>
        <Text style={styles.counterToogle}>+</Text>
      </TouchableOpacity>

      <Text style={styles.counterValue}>{props.value}</Text>
      <TouchableOpacity hitSlop={{right: 10, left: 30, top: 20, bottom: 20}} onPress={props.onDecrement}>
        <Text style={styles.counterToogle}>-</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 48,
    height: 51,
    width: 180,
    borderWidth: 1.5,
    borderColor: '#1111',
    paddingHorizontal:20
  },
  counterValue:{
    fontFamily:fonts.regular,
    fontSize:16,
    lineHeight:24,
    color: '#000000'
  },
  counterToogle:{
    color: '#73548F',
    fontFamily:fonts.regular,
    fontSize:20,
    lineHeight:30,
  }
});

export default QuantityCounter;
