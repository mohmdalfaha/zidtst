import * as React from 'react';
import {
  Text,
  ViewStyle,
  TextStyle,
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import fonts from '../assets/fonts';

export interface ActionButtonProps {
  text: string;
  textStyle?: StyleProp<TextStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  onPress?: ((event: GestureResponderEvent) => void) | undefined;
}

const ActionButton: React.FunctionComponent<ActionButtonProps> = props => {
  return (
    <TouchableOpacity style={[styles.container, props.containerStyle]}>
      <Text style={[styles.text, props.textStyle]}>{props.text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#73548F',
    height: 51,
    paddingHorizontal: 39,
    paddingVertical: 13,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 48,
  },
  text: {
    color: '#fff',
    fontFamily: fonts.regular,
    lineHeight: 24
  },
});

export default ActionButton;
