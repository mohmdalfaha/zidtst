import React, {useRef, useState} from 'react';
import type {ReactNode} from 'react';
import {View, Text, Platform, StyleSheet, Image, Animated} from 'react-native';
import images from '../assets/images';
import ActionButton from '../components/ActionButton';
import locale from '../constants/locale';
import QuantityCounter from '../components/QuantityCounter';
import LikeToggle from '../components/LikeToggle';
import ShareButton from '../components/ShareButton';
import TextLabel from '../components/TextLabel';
import fonts from '../assets/fonts';
import LabeledTextInput from '../components/LabeledTextInput';
import ListPicker from '../components/ListPicker';
import Rating from '../components/Rating';
import {BlurView} from '@react-native-community/blur';

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView);

const dummyProductDetailsData = {
  numberOfLikes: 124,
  storeName: 'Store Name',
  avatar:
    'https://assets.zid.store/themes/24c59a15-58b7-4cac-a738-0498aaaf723b/header-logo.svg',
  productName: 'Product Name Here',
  productDescription:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  price: 250,
  discounted_price: 230,
  rating: 4.3,
  shareableLink: 'https://zid.sa/',
  brand: 'CHANEL',
  sku: '1234562345678',
  size: '50 ml',
  weight: '250 g',
  colors: ['Pink', 'Red', 'Yellow'],
  warranty: '1 Year',
  isLiked: false,
};

const ProductDetails = () => {
  const [quantityValue, setQuantityValue] = useState(0);
  const [productDetailsData, setProductDetailsData] = useState(
    dummyProductDetailsData,
  );
  const [selectedColor, setSelectedColor] = useState(
    dummyProductDetailsData?.colors?.[0],
  );
  const [recipientName, setRecipientName] = useState('');

  const scrollY = useRef(new Animated.Value(0));

  const blurNavBarOpacity = scrollY.current.interpolate({
    inputRange: [0, 120],
    outputRange: [0, 0.9],
    extrapolate: 'clamp',
  });

  const storeNameAnimatedFontSize = scrollY.current.interpolate({
    inputRange: [0, 120],
    outputRange: [28, 22],
    extrapolate: 'clamp',
  });

  return (
    <View style={styles.container}>
      <AnimatedBlurView
        style={[styles.blurNavBar, {opacity: blurNavBarOpacity}]}
        blurType={ Platform.OS === 'android' ? "light" : "material"}
        blurAmount={10}
      />
      <Animated.Text
        style={[styles.storeName, {fontSize: storeNameAnimatedFontSize}]}>
        {productDetailsData.storeName}
      </Animated.Text>
      <View style={[styles.storeAvatar, {}]}>
        <Image
          style={{width: 20, height: 20}}
          source={{
            uri: 'https://zid.sa/wp-content/uploads/2022/07/zid-logo-500x271.png',
          }}
          resizeMode="contain"
        />
      </View>
      <Animated.ScrollView
        showsVerticalScrollIndicator={false}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: scrollY.current,
                },
              },
            },
          ],
          {useNativeDriver: false},
        )}
        style={styles.scrollViewContainer}>
        <Image style={styles.proudctImage} source={images.proudctImage} />
        <View style={styles.likeAndShareButtonsContainer}>
          <LikeToggle
            isLiked={productDetailsData?.isLiked}
            onToggle={() =>
              setProductDetailsData(previous => ({
                ...previous,
                isLiked: !previous?.isLiked,
              }))
            }
            likesNumber={productDetailsData?.numberOfLikes}
          />
          <ShareButton message={productDetailsData.shareableLink} />
        </View>
        <TextLabel
          text={productDetailsData.productName}
          textStyle={{
            marginTop: 8,
          }}
        />
        <View style={styles.priceAndRatingContainer}>
          <View style={styles.priceContainer}>
            {productDetailsData?.discounted_price ? (
              <Text style={styles.discountedPriceText}>
                {productDetailsData.discounted_price} SAR
              </Text>
            ) : null}
            <Text
              style={[
                styles.priceText,
                {
                  textDecorationLine: productDetailsData?.discounted_price
                    ? 'line-through'
                    : 'none',
                },
              ]}>
              {productDetailsData.price} SAR
            </Text>
          </View>
          <Rating rate={productDetailsData.rating} />
        </View>
        <Text style={styles.description}>
          {productDetailsData.productDescription}
        </Text>
        <TextLabel
          text="Details"
          textStyle={{
            marginTop: 40,
          }}
        />
        <View style={styles.specficationItemContainer}>
          <Text style={styles.specficationItemKeyText}>Brand</Text>
          <Text style={styles.specficationItemValueText}>
            {productDetailsData.brand}
          </Text>
        </View>
        <View style={styles.specficationItemContainer}>
          <Text style={styles.specficationItemKeyText}>Size</Text>
          <Text style={styles.specficationItemValueText}>
            {productDetailsData.size}
          </Text>
        </View>
        <View style={styles.specficationItemContainer}>
          <Text style={styles.specficationItemKeyText}>SKU</Text>
          <Text
            style={[
              styles.specficationItemValueText,
              {color: '#000', fontFamily: fonts.medium},
            ]}>
            {productDetailsData.sku}
          </Text>
        </View>
        <TextLabel
          text="Specfications"
          textStyle={{
            marginTop: 24,
          }}
        />
        <View style={styles.specficationItemContainer}>
          <Text style={styles.specficationItemKeyText}>Weight</Text>
          <Text style={styles.specficationItemValueText}>
            {productDetailsData.weight}
          </Text>
        </View>
        <View style={styles.specficationItemContainer}>
          <Text style={styles.specficationItemKeyText}>Color</Text>
          <Text style={styles.specficationItemValueText}>{selectedColor}</Text>
        </View>
        <View style={styles.specficationItemContainer}>
          <Text style={styles.specficationItemKeyText}>Warranty</Text>
          <Text style={styles.specficationItemValueText}>
            {productDetailsData.warranty}
          </Text>
        </View>

        <TextLabel
          text="Customization"
          textStyle={{
            marginTop: 24,
          }}
        />

        <LabeledTextInput
          onChangeText={setRecipientName}
          value={recipientName}
          placeholder="Mohammed..."
          containerStyle={{marginTop: 8}}
          label="Recipient Name"
        />

        <ListPicker
          onChange={color => {
            setSelectedColor(color);
          }}
          list={productDetailsData.colors}
          containerStyle={{marginTop: 16, width: '100%'}}
          label="Color"
          selectedItem={selectedColor}
        />

        <View
          style={{
            height: 150,
          }}
        />
      </Animated.ScrollView>
      <View style={styles.floatingContainer}>
        <QuantityCounter
          onDecrement={() =>
            setQuantityValue(previousValue =>
              previousValue !== 0 ? previousValue - 1 : previousValue,
            )
          }
          onIncreament={() =>
            setQuantityValue(previousValue => previousValue + 1)
          }
          value={quantityValue}
          containerStyle={{marginRight: 10}}
        />
        <ActionButton text={locale.BuyNow} containerStyle={{width: 143}} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollViewContainer: {
    alignSelf: 'center',
    paddingHorizontal: 16,
    backgroundColor: '#fff',
  },
  blurNavBar: {
    position: 'absolute',
    top: 0,
    zIndex: 1,
    alignSelf: 'center',
    width: '100%',
    height: 103,
  },
  storeName: {
    position: 'absolute',
    top: 60,
    left: 22,
    opacity: 1,
    zIndex: 2,
    fontFamily: fonts.semiBold,
  },
  storeAvatar: {
    position: 'absolute',
    top: 60,
    right: 22,
    width: 28,
    height: 28,
    zIndex: 2,
    opacity: 1,
    borderRadius: 28 / 2,
    backgroundColor: '##F2F2F2',
    borderWidth: 4,
    shadowOpacity: 0.06,
    borderColor: '#fff',
  },
  proudctImage: {
    width: 342,
    height: 342,
    borderRadius: 8,
    marginTop: 119,
    alignSelf: 'center',
    zIndex: 2,
  },
  likeAndShareButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 16,
  },
  priceAndRatingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 8,
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  discountedPriceText: {
    color: '#EB1747',
    fontFamily: fonts.medium,
    fontSize: 14,
    lineHeight: 21,
    marginRight: 4,
  },
  priceText: {
    color: '#522973',
    fontFamily: fonts.regular,
    fontSize: 14,
    lineHeight: 21,
  },
  description: {
    lineHeight: 21,
    fontSize: 14,
    fontFamily: fonts.regular,
    marginTop: 8,
  },
  floatingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#ffff',
    alignSelf: 'center',
    shadowOpacity: 1,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    borderTopWidth: 0.15,
    paddingVertical: 16,
    paddingHorizontal: 25,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    width: '100%',
  },
  specficationItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  specficationItemKeyText: {
    fontFamily: fonts.medium,
    color: '#545454',
    fontSize: 14,
    lineHeight: 21,
    marginTop: 8,
  },
  specficationItemValueText: {
    fontFamily: fonts.regular,
    color: '#545454',
    fontSize: 14,
    lineHeight: 21,
    marginTop: 8,
  },
});

export default ProductDetails;
