import {I18nManager} from 'react-native';
import ar from './ar';
import en from './en';

const locale = I18nManager.isRTL ? ar : en;

export default locale;
