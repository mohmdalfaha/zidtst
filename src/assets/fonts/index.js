const fonts = {
  thin: 'IBMPlexSansArabic-Thin',
  extraLight: 'IBMPlexSansArabic-ExtraLight',
  light: 'IBMPlexSansArabic-Light',
  medium: 'IBMPlexSansArabic-Medium',
  regular: 'IBMPlexSansArabic-Regular',
  semiBold: 'IBMPlexSansArabic-SemiBold',
  bold: 'IBMPlexSansArabic-Bold',
};

export default fonts;
