import React from 'react';
import type {ReactNode} from 'react';
import ProductDetails from './src/screens/ProductDetails';

const App: () => ReactNode = () => {
  return <ProductDetails />
};


export default App;
